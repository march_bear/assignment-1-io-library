section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte [rdi + rax], `\0`
        je .end
        inc rax
        jmp .loop
    .end:
        ret

print_to_stdout:
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    jmp print_to_stdout

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    call print_to_stdout
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
print_uint:
    mov r11, 10                     ; константа для деления
    mov r10, rsp                    ; сохранение значения rsp
    sub rsp, 2
    mov byte [rsp + 1], `\0`
    mov rcx, 1                      ; счетчик; обозначает количество символов, которое может быть записано на стек;
                                    ; нулевое значение сигнализирует о необходимости уменьшить rsp и обновить счетчик
    mov rax, rdi
    .loop:
        xor rdx, rdx
        div r11                     ; после деления в rdx будет лежать очередная цифра числа в двоичном представлении
        add rdx, `0`                ; перевод значения цифры в код символа ASCII
        dec rcx
        mov byte [rsp + rcx], dl
        cmp rax, 0                  ; проверка, являются ли оставшиеся разряды числа нулевыми
        je .end
        cmp rcx, 0                  ; проверка на необходимость обновить счетчик
        jne .loop
        sub rsp, 2
        mov rcx, 2
        jmp .loop
    .end:
        lea rdi, [rsp + rcx]        ; запись в rdi адреса начала числа в полученном строковом представлении
        push r10
        call print_string
        pop r10
        mov rsp, r10
        ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jns .print          ; переход, если поданное число неотрицательное

    push rdi
    mov rdi, `-`
    call print_char
    pop rdi
    neg rdi             ; получение модуля поданного числа

    .print:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov r10b, [rsi + rax]
        cmp byte [rdi + rax], r10b
        jne .ne_end
        cmp byte [rdi + rax], 0
        je .e_end
        inc rax
        jmp .loop
    .ne_end:
        xor rax, rax
        ret
    .e_end:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    sub rsp, 2
    mov byte [rsp], 0
    mov rdx, 1
    mov rsi, rsp
    xor rdi, rdi
    syscall

    mov al, byte [rsp]
    add rsp, 2
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12                    ; сохранение значений callee-saved регистров, используемых далее в функции
    push r13
    push r14
    push rdi
    mov r12, rdi
    xor r13, r13
    mov r14, rsi

    .read_spaces_loop:          ; цикл поиска первого непробельного символа в строке
        call read_char
        cmp al, ` `
        je .read_spaces_loop
        cmp al, `\t`
        je .read_spaces_loop
        cmp al, `\n`
        je .read_spaces_loop

    .read_word_loop:
        cmp r13, r14            ; проверка на наличие места в буфере
        jae .error

        cmp al, ` `
        je .rwl_end
        cmp al, `\t`
        je .rwl_end
        cmp al, `\n`
        je .rwl_end
        cmp al, `\0`
        je .rwl_end

        mov byte [r12], al

        inc r13
        inc r12

        call read_char
        jmp .read_word_loop

    .rwl_end:
        mov byte [r12], 0
        mov rdx, r13
        pop rax
    .restore_regs:              ; восстановление значений используемых callee-saved регистров и выход из функции
        pop r14
        pop r13
        pop r12
        ret
    .error:
        pop rax
        xor rax, rax
        jmp .restore_regs

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r11, r11

    cmp byte [rdi], 48                  ; реализация расчитана на то, что из строки вроде "0123..."
    je .zero                            ; должно считаться число 0 (если первый символ нуль - дальнейшее чтение
                                        ; прекращается).
    mov r10, 10                         ; константа для умножения

    .loop:
        cmp byte [rdi + rcx], '0'       ; проверка на принадлежность символа к набору от '0' до '9'
        jb .end
        cmp byte [rdi + rcx], '9'
        ja .end

        mul r10                         ; увеличение значения аккумулятора на 10 (сдвиг влево на один десятичный разряд)

        mov r11b, byte [rdi + rcx]      ; нормализуем цифру: вместо кода символа, в r11b окажется значение десятичной
        sub r11b, '0'                   ; цифры в двоичном формате
        add rax, r11                    ; прибавляем к промежуточному результату полученное после нормализации значение
        inc rcx
        jmp .loop

    .zero:
        mov rdx, 1
        ret                             ; результат: число - 0, его длина в символах - 1
    .end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '+'
    je .positive
    cmp byte [rdi], '-'
    je .negative

    jmp parse_uint          ; если первым символом не были '+' или '-', переход к функции parse_uint

    .positive:              ; если первый символ в строке - '+', производится считывание беззнакового числа
        inc rdi             ; из оставшейся части строки
        call parse_uint
        cmp rdx, 0
        je .error
        inc rdx
        ret
    .negative:              ; если первый символ в строке - '-', производится считывание беззнакового числа
        inc rdi             ; из оставшейся части строки, после чего оно преобразуется в обратное
        call parse_uint
        cmp rdx, 0
        je .error
        inc rdx
        neg rax
    .error:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx                      ; счетчик считанных символов
    .loop:
        cmp rcx, rdx                ; проверка на наличие места в буфере
        je .error
        mov r10b, byte [rdi + rcx]
        mov byte [rsi + rcx], r10b
        cmp r10b, `\0`              ; проверка на достижение конца строки
        je .end
        inc rcx
        jmp .loop
    .end:
        mov rax, rcx
    .error:
        ret
